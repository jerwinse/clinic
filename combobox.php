<?php
    include_once("define.php");
    require "KoolPHP/KoolControls/KoolComboBox/koolcombobox.php";
    require "KoolPHP/KoolControls/KoolAjax/koolajax.php";
 
    if($koolajax->isCallback)
    {
        sleep(1);
    }
 
    $selected_type_id = null;
    if(isset($_POST["tblperson_selectedValue"]))
    {
        $selected_person_id = $_POST["tblperson_selectedValue"];
    }
 
    $kcb_type = new KoolComboBox("tblperson");
    $kcb_type->scriptFolder = "KoolPHP/KoolControls/KoolComboBox/koolcombobox.php";
    $kcb_type->styleFolder="default";
    $kcb_type->width = "185px";
    $result = mysql_query("select typeID,typeName from tblperson");
    while($row= mysql_fetch_assoc($result))
    {
        $kcb_type->addItem($row["typeName"],$row["typeID"],null,($selected_type_id==$row["typeID"]));
    }
    
    
    $selected_user_id = null;
    if(isset($_POST["tbluser_selectedValue"]))
    {
        $selected_user_id = $_POST["tbluser_selectedValue"];
    }   
    
    
    $kcb_user = new KoolComboBox("tbluser");
    $kcb_user->scriptFolder = "KoolPHP/KoolControls/KoolComboBox/koolcombobox.php";
    $kcb_user->styleFolder="default";
    $kcb_user->width = "185px";
 
    if($selected_type_id!=null)
    {
        $result = mysql_query("select ID,userName from tbluser where typeID=$selected_type_id");
        while($row= mysql_fetch_assoc($result))
        {
            $kcb_user->addItem($row["userName"],$row["ID"],null,($selected_user_id==$row["ID"]));
        }       
    }
    
?>
 
<form id="form1" method="post">
    <?php echo $koolajax->Render();?>
    
    <style type="text/css">
        .background
        {
            width:745px;
            height:257px;
            background:url(images/background.png);
        }
        .break
        {
            height:140px;
        }
    </style>
 
    <?php echo KoolScripting::Start();?>
        <updatepanel id="combobox_updatepanel" class="background">
            <content>           
                <table style="width:745px;height:257px;">
                    <tr>
                        <td align="right" style="width:247px;">
                            <div class="break"></div>
                            <?php echo $kcb_type->Render();?>
                        </td>
                        <td align="right" style="width:217px;">
                            <div class="break"></div>
                            <?php echo $kcb_user->Render();?>
                        </td>
                        <!-- <td align="right" style="width:218px;">
                            <div class="break"></div>
                            <?php echo $kcb_city->Render();?>
                        </td> -->
                        <td style="width:62px;">&nbsp;</td>                     
                    </tr>
                </table>
                    
                <script type="text/javascript">
                    kcb_person.registerEvent("OnSelect",function(sender,arg){
                        combobox_updatepanel.update();
                    });
                    kcb_user.registerEvent("OnSelect",function(sender,arg){
                        combobox_updatepanel.update();
                    });                 
                </script>
            </content>
            <loading opacity="50%" image="<?php echo "KoolPHP/KoolControls/KoolAjax/loading/5.gif" ?>
        </updatepanel>
    <?php echo KoolScripting::End();?>
</form>