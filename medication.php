<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $sorting = (isset($_GET['sort']))?" order by Course asc":"";
    $sql = "SELECT * FROM " . TBL_MED. " WHERE department = ". $_SESSION['RIGHTS'] . " {$sorting}";
    $cmd->commandText = $sql;
    $res = $cmd->execute();
}
//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");?>
    
        <div id="content">
                <br/><br/>
                <form method="post">
                 <p>
                    <input type="text" name="userID" id="userID"/>
                    <input type="button" value="Search" onclick="global.search();"/>
                    <input type="button" value="Sort by Course" onclick="global.sortByCourse('medication.php');"/>
                </p>
                </form>
                <input type="button" onclick="global.formAppointment()" value="New Patient"/>
                <form method="post" action="deleteMed.php">
                <table id="large" cellspacing="0" class="tablesorter">
                    <?php 
                        if(isset($_GET['search'])){
                            require_once("search.php");
                        }
                        else{
                            echo '<thead> 
                                <tr>
                                <th>Date/Time</th>
                                <th>userID</th>
                                <th>Course</th>
                                <th>Name</th>
                                <th>Complaints</th>
                                <th>Treatment</th>
                                <th>Delete</th>
                                </tr>
                                </thead> ';
                                
                            for($i=0; $i<count($res);$i++){
                                echo '<tbody>
                                        <tr>
                                            <td>'.$res[$i]['Date'].'</td>
                                            <td>'.$res[$i]['userID'].' </td>
                                            <td>'.$res[$i]['Course'].'</td>
                                            <td>'.$res[$i]['Lname'].', '.$res[$i]['Fname'].' '.$res[$i]['Mname'].'</td>
                                            <td>'.$res[$i]['Complaints'].'</td>
                                            <td>'.$res[$i]['Treatment'].'</td>
                                            <td><input type="checkbox" name="delete[]" id="delete[]"  value="'.$res[$i]['ID'].'"></td>
                                        </tr>
                                      </tbody>';
                            }                            
                        }
                    ?>
                </table>     
                <input type="submit" name="save" id="save" value="Save Changes" />
            </form>           
            
        </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
<?endif;?>
