<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    switch ($_GET['rights']) {
        case 1:
            $pending = "SELECT * FROM " . TBL_DENTAL. " WHERE status = 'pending'";
            break;
        
        case 2:
            $pending = "SELECT * FROM " . TBL_MEDICAL. " WHERE status = 'pending'";
            break;
    }
    $cmd->commandText = $pending;
    $resPending = $cmd->execute();
}
//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");?>
    
        <div id="content">
            <form method="post" action="accept.php">
                <input type="hidden" name="department" id="department" value="<?php echo $_SESSION['RIGHTS']?>" />
            <br/><br/>
                <table>
                    <tr><th colspan="13">Pending</th></tr>
                    <tr><th colspan="13">&nbsp;</th></tr>
                    <?php
                        switch ($_SESSION['RIGHTS']) {
                            case 1:
                                  echo '
                                    <tr>
                                        <th>userID</th>
                                        <th>Course</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Date of Birth</th>
                                        <th>Good Health</th>
                                        <th>Condition</th>
                                        <th>Operation</th>
                                        <th>Accept</th>
                                        <th>Reject</th>
                                    </tr>
                                  ';
                                break;
                            
                            case 2:
                                echo '
                                    <tr>
                                        <th>Date</th>
                                        <th>userID</th>
                                        <th>Course</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Address</th>
                                        <th>Date of Birth</th>
                                        <th>Medical History</th>
                                        <th>Allergies</th>
                                        <th>Injuries</th>
                                        <th>Accept</th>
                                        <th>Reject</th>
                                    </tr>
                                ';                            
                                
                                break;
                        }
                     
                    ?>
                    <?php for($i=0; $i<count($resPending);$i++)
                        switch($_SESSION['RIGHTS']){
                            case 1:
                                echo '<tr>
                                            <td>'.$resPending[$i]['userID'].'</td>
                                            <td>'.$resPending[$i]['Course'].'</td>
                                            <td>'.$resPending[$i]['Lname'].', '.$resPending[$i]['Fname'].' '.$resPending[$i]['Mname'].'</td>
                                            <td>'.$resPending[$i]['Gender'].'</td>
                                            <td>'.$resPending[$i]['Age'].'</td>
                                            <td>'.$resPending[$i]['Address'].'</td>
                                            <td>'.$resPending[$i]['Phone'].'</td>
                                            <td>'.$resPending[$i]['DoB'].'</td>
                                            <td>'.$resPending[$i]['GH'].'</td>
                                            <td>'.$resPending[$i]['Condition'].'</td>
                                            <td>'.$resPending[$i]['Operation'].'</td>
                                            <td><input type="checkbox" name="accept[]" id="accept[]"  value="'.$resPending[$i]['ID'].'"></td>
                                            <td><input type="checkbox" name="reject[]" id="reject[]"  value="'.$resPending[$i]['ID'].'"></td>
                                      </tr>';
                                break;
                            case 2:
                                echo '<tr>
                                            <td>'.$resPending[$i]['Date'].'</td>
                                            <td>'.$resPending[$i]['userID'].'</td>
                                            <td>'.$resPending[$i]['Course'].'</td>
                                            <td>'.$resPending[$i]['Lname'].', '.$resPending[$i]['Fname'].' '.$resPending[$i]['Mname'].'</td>
                                            <td>'.$resPending[$i]['Gender'].'</td>
                                            <td>'.$resPending[$i]['Age'].'</td>
                                            <td>'.$resPending[$i]['Address'].'</td>
                                            <td>'.$resPending[$i]['DoB'].'</td>
                                            <td>'.$resPending[$i]['MedHis'].'</td>
                                            <td>'.$resPending[$i]['Med'].'</td>
                                            <td>'.$resPending[$i]['Allergies'].'</td>
                                            <td><input type="checkbox" name="accept[]" id="accept[]"  value="'.$resPending[$i]['ID'].'"></td>
                                            <td><input type="checkbox" name="reject[]" id="reject[]"  value="'.$resPending[$i]['ID'].'"></td>
                                      </tr>';
                                break;
                         }                
                    ?>
                </table>                
                <input type="submit" name="save" id="save" value="Save Changes" />
            </form>
            
        </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
<?endif;?>
