<?php 

class Class_SqlConnection
{
	# host server 
	private $host;
	# db credential
	private $databaseName;
	private $username;
	private $password;
	# for tracking purposes
	private $connection;
    	
    # constructor
	public function __construct($host=DBHOST, $username=DBUSERNAME, $password=DBPASSWORD, $databasename=DBNAME){
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->databaseName = $databasename;
	}
		
	# connect to database server
	public function connect(){
		try{
			$this->connection = @mysql_pconnect($this->host, $this->username, $this->password) or $this->throwException(mysql_error());
	        @mysql_select_db($this->databaseName, $this->connection) or $this->throwException(mysql_error());
			return $this->connection;
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	# close connection
    public function close(){
        if (isset($this->connection)){
            @mysql_close($this->connection) or $this->ThrowException(mysql_error());
            unset($this->connection);
        }
    }
    
    # validates connection
    public function isOpen(){
        return isset($this->connection);
    }

    # Exception Handler
    protected function throwException($message){
    	echo "<pre>";
    	print_r($message);
    	echo "</pre>";
    }    
	
}

?>
