<?php 


class Class_SqlCommand
{
	# assigning of statement	
	public $commandText;	
	# pass connection
	private $connection;
	
    public function __construct($connection){
    	$this->connection = $connection;
    }
        
    # returns mysql_fetch_assoc data type array # 
    public function execute(){
        try{
        	if(!is_resource($this->connection))$this->throwException("Method: Execute (Database connection is not valid)");
        	$result = @mysql_query($this->commandText, $this->connection) or $this->throwException(mysql_error());
        }
        catch(Exception $x){
        	echo $x->getMessage();
        }
        
        if (!$result){
        	return;
        }
        if(!is_resource($result)){
        	return;
        }
        else{
            $data = array();
            $i=0;
            while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
            	$data[$i] = $row;
            	$i++;
            }
            return $data;
        }     	
    }
    
    # returns mysql object
    public function executeRaw(){
        try{
    		if(!is_resource($this->connection))$this->throwException("Method: ExecuteRaw (Database connection is not valid)");
        	$obj = mysql_query($this->commandText, $this->connection) or $this->throwException(mysql_error());
        	return $obj;
        }
        catch(Exception $x){
        	echo $x->getMessage();
        }
    } 

    # returns last insert ID
    function getLastInsertID(){
        $this->commandText = "SELECT LAST_INSERT_ID() AS id";
        $row = $this->Execute();
        return $row[0]["id"];
    }

    # exception handler
    private function throwException($message){
    	echo "<pre>";
    	print_r($message);
    	echo "</pre>";
    }    
}

?>