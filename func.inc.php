<?php
include 'db.inc.php';

function search_results($userID) {
    $returned_results = array();
    $where = "";
    
    $userID = preg_split('/[\s]+/', $userID);
    $total_userID = count($userID);
    
    foreach($userID as $user=>$userid) {
        $where .= "`userID` LIKE '%$userid%'";
        if($user != ($total_userID - 1)) {
            $where .= " AND ";
        }
    }
    
    $results = "SELECT * FROM `tblmed` WHERE $where";
    $results_num = ($results = mysql_query($results)) ? mysql_num_rows($results): 0;
    
    if($results_num === 0) {
        return false;
    }else {
        while($results_row = mysql_fetch_assoc($results)) {
            $returned_results[] = array(
                'Lname' => $results_row['Lname'],
                'Fname' => $results_row['Fname'],
                'Mname' => $results_row['Mname'],
                'Course' => $results_row['Course'],
                'Date' => $results_row['Date'],
                'Complaints' => $results_row['Complaints'],
                'Treatment' => $results_row['Treatment']
            );
        }
        return $returned_results;
    }
    
}
?>