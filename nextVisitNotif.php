<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $ids = $_GET['ids'];
    $sql = "SELECT * FROM " . TBL_MED. " WHERE ID IN({$ids}) AND department = ". $_SESSION['RIGHTS'];
    $cmd->commandText = $sql;
    $res = $cmd->execute();
}
//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");?>
    
        <div id="content">
                <br/><br/>
                <!-- <input type="button" onclick="global.formMedicines()" value="New Medicine"/> -->
                <table id="large" cellspacing="0" class="tablesorter">
                    <?php 
                        if(isset($_GET['search'])){
                            require_once("search.php");
                        }
                        else{
                            echo '<thead> 
                                <tr>
                                <th>Next Visit Date</th>
                                <th>userID</th>
                                <th>Course</th>
                                <th>Name</th>
                                <th>Complaints</th>
                                <th>Treatment</th>
                                </tr>
                                </thead> ';
                                
                            for($i=0; $i<count($res);$i++){
                                echo '<tbody>
                                        <tr>
                                            <td>'.$res[$i]['NextVisitDate'].'</td>
                                            <td>'.$res[$i]['userID'].' </td>
                                            <td>'.$res[$i]['Course'].'</td>
                                            <td>'.$res[$i]['Lname'].', '.$res[$i]['Fname'].' '.$res[$i]['Mname'].'</td>
                                            <td>'.$res[$i]['Complaints'].'</td>
                                            <td>'.$res[$i]['Treatment'].'</td>
                                        </tr>
                                      </tbody>';
                            }                            
                        }
                    ?>
                </table>                
            
        </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
<?endif;?>
