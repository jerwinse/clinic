<?php
require_once('define.php');

if(isset($_POST)){
    if(isset($_POST['delete'])){
        $conObj = new Class_SqlConnection();
        $con = $conObj->connect();
        $cmd = new Class_SqlCommand($con, "");
        
        $tbl = $_POST['department']==1?TBL_MED:TBL_MED;
        $ids = implode(",", $_POST['delete']);
        $sql = "DELETE FROM {$tbl} WHERE ID IN({$ids})";
        $cmd->commandText = $sql;
        $cmd->execute();
    }
    
}
header("location: medication.php");

?>