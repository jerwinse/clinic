<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $sql = "SELECT * FROM " . TBL_MEDICINES. " WHERE department = ". $_SESSION['RIGHTS']. " AND ID IN(" . $_GET['ids'] . ")";
    $cmd->commandText = $sql;
    $res = $cmd->execute();
}
//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");?>
    
        <div id="content">
                <br/><br/>
                <table>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Consumed</th>
                        <th>Balance</th>
                        <th>Expiration</th>
                        <th>Edit</th>
                    </tr>
                    <?php 
                        for($i=0; $i<count($res);$i++){
                            $balance = $res[$i]['Quantity'] - $res[$i]['Consumed']; 
                            echo '<tr>
                                        <td>'.$res[$i]['Date'].'</td>
                                        <td>'.$res[$i]['Name'].'</td>
                                        <td>'.$res[$i]['Quantity'].'</td>
                                        <td>'.$res[$i]['Consumed'].'</td>
                                        <td>'.$balance.'</td>
                                        <td>'.$res[$i]['Expiration'].'</td>
                                        <td><a href="medicineForm.php?id='.$res[$i]['ID'].'">[ edit ]</a></td>
                                  </tr>';
                        }
                    ?>
                </table>                
            
        </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
<?endif;?>
