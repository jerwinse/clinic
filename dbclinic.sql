-- MySQL dump 10.13  Distrib 5.5.20, for Win32 (x86)
--
-- Host: localhost    Database: dbclinic
-- ------------------------------------------------------
-- Server version	5.5.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `searchbox`
--

DROP TABLE IF EXISTS `searchbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `avatarloc` varchar(220) NOT NULL,
  `avator` enum('0','1') NOT NULL DEFAULT '0',
  `showing` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchbox`
--

LOCK TABLES `searchbox` WRITE;
/*!40000 ALTER TABLE `searchbox` DISABLE KEYS */;
INSERT INTO `searchbox` VALUES (1,'test','','0','1'),(2,'beef','image/girl.png','1','1');
/*!40000 ALTER TABLE `searchbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldental`
--

DROP TABLE IF EXISTS `tbldental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldental` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` varchar(50) NOT NULL,
  `typeID` varchar(50) NOT NULL,
  `Course` varchar(50) NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Fname` varchar(50) NOT NULL,
  `Mname` varchar(50) NOT NULL,
  `Gender` varchar(50) NOT NULL,
  `Age` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `DoB` date NOT NULL,
  `PoB` varchar(50) NOT NULL,
  `Guardian` varchar(50) NOT NULL,
  `GH` varchar(50) NOT NULL,
  `Med` varchar(50) NOT NULL,
  `Condition` varchar(50) NOT NULL,
  `Illness` varchar(50) NOT NULL,
  `Operation` varchar(50) NOT NULL,
  `Hospi` varchar(50) NOT NULL,
  `Hospitalized` varchar(50) NOT NULL,
  `Pres` varchar(50) NOT NULL,
  `Prescription` varchar(50) NOT NULL,
  `Preg` varchar(50) NOT NULL,
  `Allergic` varchar(50) NOT NULL,
  `BP` varchar(50) NOT NULL,
  `DoH` varchar(50) NOT NULL,
  `Status` varchar(20) DEFAULT 'pending',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldental`
--

LOCK TABLES `tbldental` WRITE;
/*!40000 ALTER TABLE `tbldental` DISABLE KEYS */;
INSERT INTO `tbldental` VALUES (1,'2009-00039-TG-0','1','BSIT 3-1','Martinez','Meryl','Espiritu','female','19','8 Buli, Muntinlupa City','09486253330','1993-03-14','Muntinlupa, City','Lolita Martinez','yes','assa','asa','gg','hghg','g','ggh','ghgh','ghg','hghg','hghg','ghg','hgh','pending'),(2,'','','','assa','sas','ass','female','12','sas','1221','1993-10-12','assa','ass','no','no','sasa','no','ass','no','ass','no','ass','no','sas','212','sas','accepted'),(3,'','','','epiritu','jerwin','silvestre','Male','33','taguig','11111111111','1993-03-14','taguig','test','Yes','Yes','','Yes','','Yes','','Yes','','Yes','a','','g','pending'),(4,'','','','last','first','middle','Male','22','address','1111111111','1993-03-14','taguig','parent','No','No','','No','','No','','No','','No','a','','g','accepted'),(5,'','','','','','','','','','','0000-00-00','','','','','','','','','','','','','','','','pending'),(6,'','','','','','','','','','','0000-00-00','','','','','','','','','','','','','','','','pending'),(7,'','','','','','','','','','','0000-00-00','','','','','','','','','','','','','','','','pending'),(8,'','','','','','','Female','','','','0000-00-00','','','Yes','Yes','','Yes','','Yes','','Yes','','Yes','','','','pending'),(9,'2009-00039-TG-0','1','BSIT 3-1','Martinez','Meryl','Espiritu','female','19','8 Buli, Muntinlupa City','09486253330','1993-03-14','Muntinlupa, City','Lolita Martinez','no','ffjf','kjh','dt','dgd','h','jfgjh','gggjhg','jhgjgjg','ghgjhg','jgjhgjh','ggjh','jghj','pending'),(10,'2009-00004-TG-0','1','BSIT 3-1','Dela Cruz ','Lara Mae','San Jose','female','19','Parañaque City','02121','1992-12-12','Parañaque City','hkjh','khk','hkjh','khjk','hkhkh','khkjh','khk','hkh','kh','khkh','khk','hkh','jkhkjhj','hjkh','pending'),(11,'','','','Martinez','Meryl','Espiritu','Female','19','8 buli muntinlupa city','09304917524','1993-03-14','Muntinlupa City','Lolita E. Martinez','Yes','Yes','assa','Yes','sasa','Yes','sasa','Yes','sasa','Yes','as','23','as','pending');
/*!40000 ALTER TABLE `tbldental` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmed`
--

DROP TABLE IF EXISTS `tblmed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblmed` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` varchar(50) NOT NULL,
  `Course` varchar(50) NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Fname` varchar(50) NOT NULL,
  `Mname` varchar(50) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NextVisitDate` datetime DEFAULT NULL,
  `Complaints` varchar(50) NOT NULL,
  `Treatment` varchar(50) NOT NULL,
  `Department` tinyint(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmed`
--

LOCK TABLES `tblmed` WRITE;
/*!40000 ALTER TABLE `tblmed` DISABLE KEYS */;
INSERT INTO `tblmed` VALUES (9,'2009-00004-TG-0','BSIT 3-1','Dela Cruz','Lara Mae','San Jose','2012-05-11 16:00:00',NULL,'Headache','Biogesic',2),(12,'2009-00039-TG-1','BSIT 3-1','test','test','test','2012-05-12 16:00:00','2012-05-20 00:00:00','test','test',2),(13,'2009-00004-TG-0','BSIT 3-1','Dela Cruz','Lara Mae','San Jose','2012-05-12 16:00:00','2012-05-15 00:00:00','LBM','Diatabs',2),(14,'2011-00011-TG-0','BSME 1-1','Aquino','Mon Aldrin','Espiritu','2012-05-12 16:00:00','2012-05-16 00:00:00','Headache','Biogesic',2),(15,'2011-00425-TG-0','BOA 1-1','test','test','test','2012-05-12 16:00:00','2012-05-15 00:00:00','test','Biogesic',2),(16,'2009-00039-TG-0','BSIT 3-1','Martinez','Meryl','Espiritu','2012-05-13 16:00:00','2012-06-21 16:00:00','Headache','Biogesic',2),(17,'2009-00004-TG-0','BSIT 3-1','Dela Cruz','Lara Mae','San Jose','2012-05-13 16:00:00','2012-05-31 00:30:00','Headache','Biogesic',2),(18,'2009-00048-TG-0','BSIT 3-1','saas','ghjg','ghg','2012-05-09 16:00:00','2012-05-13 06:00:00','hghg','hgh',2);
/*!40000 ALTER TABLE `tblmed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmedical`
--

DROP TABLE IF EXISTS `tblmedical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblmedical` (
  `ID` varchar(30) NOT NULL,
  `userID` varchar(50) NOT NULL,
  `typeID` varchar(50) NOT NULL,
  `Course` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Fname` varchar(50) NOT NULL,
  `Mname` varchar(50) NOT NULL,
  `Gender` varchar(50) NOT NULL,
  `Age` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `DoB` date NOT NULL,
  `PoB` varchar(50) NOT NULL,
  `Guardian` varchar(50) NOT NULL,
  `MedHis` varchar(50) NOT NULL,
  `Others` varchar(50) NOT NULL,
  `Med` varchar(50) NOT NULL,
  `Allergies` varchar(50) NOT NULL,
  `Injuries` varchar(50) NOT NULL,
  `Operation` varchar(50) NOT NULL,
  `Handi` varchar(50) NOT NULL,
  `Mens` varchar(50) NOT NULL,
  `Lmens` varchar(50) NOT NULL,
  `Status` varchar(20) DEFAULT 'pending',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmedical`
--

LOCK TABLES `tblmedical` WRITE;
/*!40000 ALTER TABLE `tblmedical` DISABLE KEYS */;
INSERT INTO `tblmedical` VALUES ('107','','',2,'0000-00-00','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','accepted'),('108','','',2,'2012-05-05','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','accepted'),('109','','',2,'2012-05-06','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','accepted'),('110','','',2,'2012-05-06','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','accepted'),('113','','',1,'2012-05-06','Martinez','Meryl','Espiritu','Female','22','ssa','1993-03-14','sa','sas','','','','','','','','undefined','','accepted'),('116','','',2,'2012-05-06','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','pending'),('2','','',2,'2012-05-05','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','accepted'),('21','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','pending'),('22','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('23','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('24','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('25','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('26','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('27','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('28','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('29','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('30','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('31','','',2,'2012-05-04','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('32','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('33','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('34','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('35','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('36','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('37','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('38','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('39','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('40','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('41','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('42','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('43','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('44','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('45','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('46','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('47','','',2,'2012-05-05','','','','','','','0000-00-00','','','','','','','','','','','','accepted'),('48','','',2,'2012-05-05','','','','undefined','','','0000-00-00','','','','','','','','','','Regular','','accepted'),('49','','',2,'2012-05-05','d','d','d','Male','d','dasa','0000-00-00','d','d','d','d','d','d','d','d','d','Regular','d','accepted'),('50','','',2,'2012-05-05','d','d','d','Male','d','dasa','0000-00-00','d','d','d','d','d','d','d','d','d','Regular','d','accepted'),('51','','',2,'2012-05-05','d','d','d','Male','d','dasa','0000-00-00','d','d','d','d','d','d','d','d','d','Regular','d','accepted'),('52','','',2,'2012-05-05','d','d','d','Male','d','dasa','0000-00-00','d','d','d','d','d','d','d','d','d','Regular','d','accepted'),('53','','',2,'2012-05-05','d','d','d','Male','d','dasa','0000-00-00','d','d','d','d','d','d','d','d','d','Regular','d','accepted'),('54','','',2,'2012-05-05','d','d','d','Male','d','dasa','0000-00-00','d','d','d','d','d','d','d','d','d','Regular','d','accepted'),('55','','',2,'2012-05-05','','','','undefined','','','0000-00-00','','','','','','','','','','undefined','','accepted'),('56','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Regular','','accepted'),('57','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Regular','','accepted'),('58','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('59','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('60','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('61','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('62','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('63','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('64','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('65','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('66','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('67','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('68','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('69','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('70','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('71','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('72','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('73','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('74','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('75','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('76','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('77','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('78','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('79','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('80','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('81','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('82','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('83','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('84','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('85','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('86','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('87','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('88','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('89','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('90','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('91','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('92','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('93','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('94','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('95','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('96','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('97','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('98','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted'),('99','','',2,'2012-05-05','','','','Male','','','0000-00-00','','','','','','','','','','Irregular','','accepted');
/*!40000 ALTER TABLE `tblmedical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmedicines`
--

DROP TABLE IF EXISTS `tblmedicines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblmedicines` (
  `ID` int(9) NOT NULL AUTO_INCREMENT,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Name` varchar(255) DEFAULT NULL,
  `Quantity` decimal(9,2) DEFAULT '0.00',
  `Consumed` decimal(9,2) DEFAULT '0.00',
  `minQuantity` decimal(9,2) DEFAULT '0.00',
  `Department` tinyint(1) DEFAULT NULL,
  `Expiration` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmedicines`
--

LOCK TABLES `tblmedicines` WRITE;
/*!40000 ALTER TABLE `tblmedicines` DISABLE KEYS */;
INSERT INTO `tblmedicines` VALUES (1,'2012-05-05 09:14:33','Loperamide cap',98.00,20.00,10.00,2,'2012-05-13'),(2,'2012-05-05 09:14:36','Colvan cap',258.00,35.00,10.00,2,'2012-05-20'),(3,'2012-05-05 09:14:40','Celestamine',10.00,1.00,5.00,2,'2012-05-22'),(4,'2012-05-05 09:14:46','Fermin',0.50,0.00,2.00,1,'2012-05-28'),(5,'2012-05-05 09:14:51','Pana Spray',100.00,0.00,2.00,1,'2012-05-29'),(6,'2012-05-05 09:14:56','Celluloid',8.00,0.00,2.00,1,'2012-05-06'),(7,'0000-00-00 00:00:00','name',1.00,2.00,3.00,1,'2011-11-01'),(8,'2012-05-04 16:00:00','Cocoa Butter',1.00,0.00,1.00,1,'2012-05-12'),(9,'2012-05-04 16:00:00','Suture',1.00,0.00,1.00,1,'2012-05-13'),(10,'2012-05-04 16:00:00','Cotton',20.00,0.00,10.00,1,'2012-05-30'),(14,'2012-05-04 16:00:00','Amoxicillin 500mg',65.00,40.00,20.00,1,'2012-05-20'),(26,'2012-05-04 16:00:00','Cafelexin 500mg',60.00,40.00,50.00,1,'2012-05-30');
/*!40000 ALTER TABLE `tblmedicines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblperson`
--

DROP TABLE IF EXISTS `tblperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblperson` (
  `typeID` varchar(30) NOT NULL,
  `typeName` varchar(50) NOT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblperson`
--

LOCK TABLES `tblperson` WRITE;
/*!40000 ALTER TABLE `tblperson` DISABLE KEYS */;
INSERT INTO `tblperson` VALUES ('1','student'),('2','faculty');
/*!40000 ALTER TABLE `tblperson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbluser`
--

DROP TABLE IF EXISTS `tbluser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbluser` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `userID` varchar(50) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `typeID` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2012 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbluser`
--

LOCK TABLES `tbluser` WRITE;
/*!40000 ALTER TABLE `tbluser` DISABLE KEYS */;
INSERT INTO `tbluser` VALUES (1,'','test',''),(2009,'','BSIT 3-1','1'),(2010,'2009-00039-TG-0','BSIT 3-1','1'),(2011,'2009-00004-TG-0','BSIT 3-1','1');
/*!40000 ALTER TABLE `tbluser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `department` tinyint(2) DEFAULT NULL,
  `isEnabled` tinyint(1) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Nelson P. Angeles','dental','81dc9bdb52d04dc20036dbd8313ed055',1,1,'2012-05-06 00:34:28'),(2,'Joyce S. Reyes','medical','202cb962ac59075b964b07152d234b70',2,1,'2012-05-06 00:35:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-05-16 18:00:01
