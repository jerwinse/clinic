<h3 style="font-size:20px;">Consultation</h3>
<label class="textTbl"></label>
<table style="width:60%;">
	<tr>
		<th colspan="3">Medical History</th>
	</tr>
	<tr>
		<td><label class="textTbl">Are you in good health?</label></td><td><input type="radio" name="gh" id="gh" value="yes"/>Yes</td><td><input type="radio" name="gh" id="gh" value="no"/>No</td>
	</tr>
	<tr>
		<td><label class="textTbl">Are you under medication?</label></td><td><input type="radio" name="med" id="med" value="yes"/>Yes</td><td><input type="radio" name="med" id="med" value="no"/>No</td>
	</tr>
	<tr>
		<td><label class="textTbl">If so, condition being treated?</label>&nbsp;&nbsp;</td><td colspan="2"><input style="width:300px;" type="text" name="medMsg" id="medMsg"></td>
	</tr>
	<tr>
		<td><label class="textTbl">Have you ever had serious illness></label></td></td><td><input type="radio" name="illness" id="illness" value="yes"/>Yes</td><td><input type="radio" name="illness" id="illness" value="no"/>No</td>		
	</tr>
	<tr>
		<td><label class="textTbl">If so, what illness or operation?</label>&nbsp;&nbsp;</td><td colspan="2"><input style="width:300px;" type="text" name="illnessMsg" id="illnessMsg"></td>
	</tr>
	<tr>
		<td><label class="textTbl">Have you ever been hospitalized?</label></td><td><input type="radio" name="hospitalized" id="hospitalized" value="yes"/>Yes</td><td><input type="radio" name="hospitalized" id="hospitalized" value="no"/>No</td>
	</tr>
	<tr>
		<td><label class="textTbl">If so, why and when?</label>&nbsp;&nbsp;</td><td colspan="2"><input style="width:300px;" type="text" name="hospitalizedMsg" id="hospitalizedMsg"></td>
	</tr>
	<tr>
		<td><label class="textTbl">Are you taking prescriptions?</label></td><td><input type="radio" name="prescription" id="prescription" value="yes"/>Yes</td><td><input type="radio" name="prescription" id="prescription" value="no"/>No</td>
	</tr>
	<tr>
		<td><label class="textTbl">If so, please specify</label>&nbsp;&nbsp;</td><td colspan="2"><input style="width:300px;" type="text" name="prescriptionMsg" id="prescriptionMsg"></td>
	</tr>
	<tr>
		<td><label class="textTbl">Are you pregnant?</label></td><td><input type="radio" name="pregnant" id="pregnant" value="yes"/>Yes</td><td><input type="radio" name="pregnant" id="pregnant" value="no"/>No</td>
	</tr>
	<tr>
		<td colspan="3">
			<table>
				<tr>
					<th colspan="3">Allergic to any of the following</th>
				</tr>
				<tr>
					<td><input type="checkbox" name="anesthetic" id="anesthetic"/> Local Anesthetic</td>
					<td><input type="checkbox" name="aspirin" id="aspirin"/> Penicillin Antibiotic</td>
					<td><input type="checkbox" name="nsaid" id="nsaid"/> NSAID</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="antibiotic" id="antibiotic"/> Penicillin Antibiotic</td>
					<td><input type="checkbox" name="latex" id="latex"/> Local Anesthetic</td>
					<td><input type="checkbox" name="other" id="other"/> NSAID</td>
				</tr>
				<tr>
					<td><label class="textTbl">Specify</label></td><td colspan="2" style="text-align:left;"><input style="width:300px;" type="text" name="specificAllergy" id="specificAllergy"/></td>
				</tr>
				<tr>
					<td><label class="textTbl">Serious Illness</label></td><td colspan="2" style="text-align:left;"><input style="width:300px;" type="text" name="seriousIllness" id="seriousIllness"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><label class="textTbl">Findings</label></td>
		<td colspan="2"><textarea name="findings" id="findings" style="width:415px;align:right;"></textarea></td>
	</tr>
	<tr>
		<td><label class="textTbl">Medicine</label></td><td colspan="2"><input type="text" name="medicine" id="medicine"/></td>
	</tr>
	<tr>
		<td><label class="textTbl">Every</label></td><td colspan="2"><input type="text" name="medicineTake" id="medicineTake"/></td>
	</tr>
	<tr>
		<td><label class="textTbl">Adviced to undergo</label></td>
		<td colspan="2">
			<select name="adviced" id="adviced" class="selmenu" style="width:200px;">
				<option value="extraction">Tooth Extraction</option>
				<option value="cleaning">Cleaning</option>
				<option value="pasta">Pasta</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label class="textTbl">Other</label></td><td colspan="2"><input type="text" name="other" id="other"/></td>
	</tr>
	<tr>
		<td><label class="textTbl">Next Visit Date</label></td><td colspan="2"><input type="text" id="NextVisitDate" name="NextVisitDate"/></td>
	</tr>
	<tr>
		<td><label class="textTbl">Time</label></td><td colspan="2"><input type="text" name="other" id="other"/></td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:center;"><input type="button" value="Save" class="buttonSave" onclick="global.saveConsultation();"/></td>
	</tr>
</table>