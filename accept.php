<?php
require_once('define.php');


if(isset($_POST)){
    if(isset($_POST['accept'])){
        $conObj = new Class_SqlConnection();
        $con = $conObj->connect();
        $cmd = new Class_SqlCommand($con, "");
        
        $tbl = $_POST['department']==1?TBL_DENTAL:TBL_MEDICAL;
        $ids = implode(",", $_POST['accept']);
        $sql = "UPDATE {$tbl} SET Status = 'accepted' WHERE ID IN({$ids})";
        $cmd->commandText = $sql;
        $cmd->execute();
    }
    
    if(isset($_POST['reject'])){
        $conObj = new Class_SqlConnection();
        $con = $conObj->connect();
        $cmd = new Class_SqlCommand($con, "");
        
        $tbl = $_POST['department']==1?TBL_DENTAL:TBL_MEDICAL;
        $ids = implode(",", $_POST['reject']);
        $sql = "DELETE FROM {$tbl} WHERE ID IN({$ids})";
        $cmd->commandText = $sql;
        $cmd->execute();
    }
     
}
header("location: index.php");

?>