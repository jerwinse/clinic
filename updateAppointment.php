<?php

$res = array();
$res['message'] = "Error on updating!";

if(isset($_POST)){
    if($_POST['id']){
        require_once("define.php");
        
        $conObj = new Class_SqlConnection();
        $con = $conObj->connect();
        $cmd = new Class_SqlCommand($con,"");
        
        $id = isset($_POST['id'])?$_POST['id']:"";
        $dept = isset($_POST['dept'])?$_POST['dept']:"";
        $date = isset($_POST['date'])?$_POST['date']:"";
        $NextVisitDate = isset($_POST['NextVisitDate'])?$_POST['NextVisitDate']:"";
        $userID = isset($_POST['userID'])?$_POST['userID']:"";
        $Course = isset($_POST['Course'])?$_POST['Course']:"";
        $Lname = isset($_POST['Lname'])?$_POST['Lname']:"";
        $Fname = isset($_POST['Fname'])?$_POST['Fname']:"";
        $Mname = isset($_POST['Mname'])?$_POST['Mname']:"";
        $Complaints = isset($_POST['Complaints'])?$_POST['Complaints']:"";
        $Treatment = isset($_POST['Treatment'])?$_POST['Treatment']:"";
        $dept = isset($_POST['dept'])?$_POST['dept']:"";
    
        $cmd->commandText = "UPDATE tblmed SET Date = '{$date}', NextVisitDate = '{$NextVisitDate}', userID = '{$userID}', Course = '{$Course}', Lname = '{$Lname}',Fname = '{$Fname}',Mname = '{$Mname}',Complaints = '{$Complaints}',Treatment = '{$Treatment}', Department = {$dept} WHERE ID = {$id} LIMIT 1";
        $cmd->execute();
        $res['message'] = "Changes has been saved successfully!";        
    }
}

print_r(json_encode($res));

?>