<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){    
	if(isset($_POST['usernameText']) && isset($_POST['passwordText'])){	
		$conObj = new Class_SqlConnection();
		$con = $conObj->connect();
		$cmd = new Class_SqlCommand($con);
		$username = strtolower(trim($_POST['usernameText']));
		$password = md5(trim($_POST['passwordText']));
		$sqlUsername = "SELECT isEnabled, department, fullname FROM users WHERE username = '{$username}' AND password = '{$password}'";
		$cmd->commandText = $sqlUsername;
		$resultAcount = $cmd->execute();
		
		if(!empty($resultAcount)){
			if($resultAcount['0']['isEnabled'] == "1"){
                $rights = $resultAcount['0']['department'];
				$_SESSION['SID'] = $password;
                $_SESSION['RIGHTS'] = $rights;
                $_SESSION['fullname'] = $resultAcount[0]['fullname'];
                # department
                # 1 = dental
                # 2 = medical
                $sorting = (isset($_GET['sort']))?" order by Course asc":"";
                switch ($_SESSION['RIGHTS']) {
                    case 1:
                        $accepted = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'accepted' {$sorting}";
                        $pending = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'pending'";
                        break;
                    
                    case 2:
                        $accepted = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'accepted' {$sorting}";
                        $pending = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'pending'";
                        
                        break;
                }
                $cmd->commandText = $accepted;
                $res = $cmd->execute(); 
                $cmd->commandText = $pending;
                $resPending = $cmd->execute();
			}
			else{
				$_SESSION['message'] = "Your account is disabled";
				header("Location:login.php");
			}
		}
		else{
			$_SESSION['message'] = "Invalid Username / Password";		
			header("Location:login.php");
		}
	}
	else{
		$_SESSION['message'] = "Enter Username / Password";
		header("Location:login.php");
	}
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $sorting = (isset($_GET['sort']))?" order by Course asc":"";        
    switch ($_SESSION['RIGHTS']) {
        case 1:
            $accepted = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'accepted' {$sorting}";
            $pending = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'pending'";
            break;
        
        case 2:
            $accepted = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'accepted' {$sorting}";
            $pending = "SELECT * FROM " . TBL_APPOINTMENT . " WHERE Status = 'pending'";
            break;
    }
    $cmd->commandText = $accepted;
    $res = $cmd->execute();	
    $cmd->commandText = $pending;
    $resPending = $cmd->execute();
}
//else{
//	$_SESSION['message'] = "Enter Username / Password";
//	header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>	
	<!-- Tab Menu -->
	<?php include_once('variables/tabmenu.php');?>
	
	<h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
	
	<div id="inside">
	
	<!-- Side Menu -->
	<?php include_once("variables/sidemenu.php");?>
	
		<div id="content">
		    <br/><br/>
                <form method="post">
                 <p>
                    <input type="text" name="userID" id="userID"/>
                    <input type="button" value="Search" onclick="global.search();"/>
                    <input type="button" value="Sort by Course" onclick="global.sortByCourse('index.php');"/>
                </p>
                </form>
		    <!-- <input type="button" onclick="global.formAppointment()" value="New Patient"/> -->
		    <form method="post" action="deleteApp.php">
                <table>
                    
                    <?php
                    if(isset($_GET['search'])){
                        require_once("search.php");
                    }
                    else {
                        echo '<tr><th colspan="13">Scheduled Appointments</th></tr>';                                        
                        switch ($_SESSION['RIGHTS']) {
                            case 1:
                                echo '
                                    <tr>
                                        <th>DateTime</th>
                                        <th>User Id</th>
                                        <th>Name</th>
                                        <th>Course</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>Add Medication</th>
                                    </tr>
                                ';
                                break;
                            
                            case 2:
                                echo '
                                    <tr>
                                        <th>DateTime</th>
                                        <th>User Id</th>
                                        <th>Name</th>
                                        <th>Course</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        <th>Add Medication</th>
                                    </tr>
                                ';                            
                                break;
                        }
                     }
                    ?>
                    <?php 
                    if(isset($_GET['search'])){
                    }
                    else {
                        for($i=0; $i<count($res);$i++)
                        switch($_SESSION['RIGHTS']){
                            case 1:
                                echo '<tr>
                                            <td>'.$res[$i]['DateTime'].'</td>
                                            <td>'.$res[$i]['userID'].'</td>
                                            <td>'.$res[$i]['Name'].'</td>
                                            <td>'.$res[$i]['Course'].'</td>
                                            <td><a href="javascript:;" onclick="global.redirectUrl(\'editMedication.php\');">Edit</a></td>
                                            <td><input type="checkbox" name="delete[]" id="delete[]"  value="'.$res[$i]['ID'].'"></td>
                                            <td><a href="javascript:;" onclick="global.redirectUrl(\'addMedication.php\');">Add</a></td>
                                      </tr>';
                                break;
                            case 2:
                                echo '<tr>
                                            <td>'.$res[$i]['DateTime'].'</td>
                                            <td>'.$res[$i]['userID'].'</td>
                                            <td>'.$res[$i]['Name'].'</td>
                                            <td>'.$res[$i]['Course'].'</td>
                                            <td><a href="javascript:;" onclick="global.redirectUrl(\'editMedication.php\');">Edit</a></td>
                                            <td><input type="checkbox" name="delete[]" id="delete[]"  value="'.$res[$i]['ID'].'"></td>
                                            <td><a href="javascript:;" onclick="global.redirectUrl(\'addMedication.php?id='.$res[$i]['userID'].'\');">Add</a></td>
                                      </tr>';
                                break;
                         }
                      }            
                    ?>
                </table>  
                <input type="submit" name="save" id="save" value="Save Changes" />
            </form>                   
		</div>
	</div><!-- end of inside -->
	
	
	
	<!-- Footer -->
	<?php include_once('variables/footer.php');?>
	</body>
	</html>
	
<?endif;?>
