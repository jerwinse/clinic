var global = {
	formMedicines: function(){
		window.location.href="medicineForm.php";
	},
	saveMedicine: function(){
		if($("#name").val().length > 0 && $("#date").val().length > 0  && $("#quantity").val().length > 0 && $("#consumed").val().length > 0 && $("#minQuantity").val().length > 0  && $("#expiration").val().length > 0)
		{
			$.ajax({
				type:'post',
				url:'saveMedicine.php',
				dataType:'json',
				data:{
					"dept":$("#dept").val(),
					"date":$("#date").val(),
					"name":$("#name").val(),
					"quantity":$("#quantity").val(),
					"consumed":$("#consumed").val(),
					"minQuantity":$("#minQuantity").val(),
					"dept":$("#dept").val(),
					"expiration":$("#expiration").val(),
				},
				success:function(res){
					alert(res.message);
					document.getElementById('name').value = '';
					document.getElementById('quantity').value = '';
					document.getElementById('consumed').value = '';
					document.getElementById('minQuantity').value = '';
					document.getElementById('expiration').value = '';
				}
			});
		}else{
			alert("Fill all fields");
			
		}
	},
	updateMedicine: function(){
		if($("#name").val().length > 0 && $("#date").val().length > 0  && $("#quantity").val().length > 0 && $("#consumed").val().length > 0 && $("#minQuantity").val().length > 0  && $("#expiration").val().length > 0)
		{
			$.ajax({
				type:'post',
				url:'updateMedicine.php',
				dataType:'json',
				data:{
					"id":$("#id").val(),
					"dept":$("#dept").val(),
					"date":$("#date").val(),
					"name":$("#name").val(),
					"quantity":$("#quantity").val(),
					"consumed":$("#consumed").val(),
					"minQuantity":$("#minQuantity").val(),
					"dept":$("#dept").val(),
					"expiration":$("#expiration").val(),
				},
				success:function(res){
					alert(res.message);
					window.location.href="medicineForm.php?id=" + $("#id").val();
				}
			});
		}else{
			alert("Fill all fields");
			window.location.href="medicineForm.php?id=" + $("#id").val();
		}
	},
	
	//medical appointment
	formAppointment: function(){
		window.location.href="appointmentForm.php";
	},
	saveAppointment: function(){
		if( $("#date").val().length > 0 &&  
			$("#NextVisitDate").val().length > 0 && 
			$("#userID").val().length > 0  && 
			$("#Course").val().length > 0 && 
			$("#Lname").val().length > 0 && 
			$("#Fname").val().length > 0  && 
			$("#Mname").val().length > 0 && 
			$("#Complaints").val().length > 0 
			&& $("#Treatment").val().length > 0)
		{
			$.ajax({
				type:'post',
				url:'saveAppointment.php',
				dataType:'json',
				data:{
					"dept":$("#dept").val(),
					"hour":$("#hour").val(),
					"date":$("#date").val(),
					"userID":$("#userID").val(),
					"Course":$("#Course").val(),
					"Lname":$("#Lname").val(),
					"Fname":$("#Fname").val(),
					"Mname":$("#Mname").val(),
					"Complaints":$("#Complaints").val(),
					"Treatment":$("#Treatment").val(),
					"dept":$("#dept").val(),
					"NextVisitDate":$("#NextVisitDate").val(),
				},
				success:function(res){
					alert(res.message);
					document.getElementById('NextVisitDate').value = '';
					document.getElementById('userID').value = '';
					document.getElementById('Course').value = '';
					document.getElementById('Lname').value = '';
					document.getElementById('Fname').value = '';
					document.getElementById('Mname').value = '';
					document.getElementById('Complaints').value = '';
					document.getElementById('Treatment').value = '';
				}
			});
		}else{
			alert("Fill all fields");
			
		}
	},
	updateAppointment: function(){
		if($("#Date").val().length > 0 && $("#NextVisitDate").val().length > 0 && $("#userID").val().length > 0  && $("#Course").val().length > 0 && $("#Lname").val().length > 0 && $("#Fname").val().length > 0  && $("#Mname").val().length > 0 && $("#Complaints").val().length > 0 && $("#Treatment").val().length > 0)
		{
			$.ajax({
				type:'post',
				url:'updateAppointment.php',
				dataType:'json',
				data:{
					"id":$("#id").val(),
					"dept":$("#dept").val(),
					"date":$("#date").val(),
					"NextVisitDate":$("#NextVisitDate").val(),
					"lname":$("#userID").val(),
					"fname":$("#Course").val(),
					"mname":$("#Lname").val(),
					"gender":$("#Fname").val(),
					"age":$("#Mname").val(),
					"address":$("#Complaints").val(),
					"dob":$("#Treatment").val(),
				},
				success:function(res){
					alert(res.message);
					window.location.href="appointmentForm.php?id=" + $("#id").val();
				}
			});
		}else{
			alert("Fill all fields");
			window.location.href="appointmentForm.php?id=" + $("#id").val();
		}
	},
	search:function(){
		window.location.href="index.php?search=" + $("#userID").val();
	},
	sortByCourse:function(page){
		window.location.href=page+"?sort=course";
	},
	redirectUrl:function(url){
		window.location.href=url;
	}
}
