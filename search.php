<?php
    require_once("define.php");
     
    $userID = $_GET['search'];

    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $sql = "SELECT * FROM " . TBL_MED. " WHERE userID = '{$userID}'";
    $cmd->commandText = $sql;
    $res = $cmd->execute();
    if(!empty($res)){
        $userName = "{$res[0]['userID']} : {$res[0]['Lname']}, {$res[0]['Fname']} {$res[0]['Mname']}";
        echo '
            <tr><th colspan="12">'.$userName.'</th></tr>
            <tr>
                <th>Date/Time</th>
                <th>Course</th>
                <th>Complaints</th>
                <th>Treatment</th>
            </tr>
        ';
        for($i=0; $i<count($res); $i++){
            echo "<tr>";
            echo "<td>{$res[$i]['Date']}</td>";
            echo "<td>{$res[$i]['Course']}</td>";
            echo "<td>{$res[$i]['Complaints']}</td>";
            echo "<td>{$res[$i]['Treatment']}</td>";
            echo "</tr>";
        }        
    }
    else {
        // echo "<tr><td colspan=\"12\">No record found</td></tr>";
        echo "<br/>";
        echo "<h2>No record found</h2>";
    }
?>
