<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $sql = "SELECT * FROM " . TBL_MEDICINES. " WHERE department = ". $_SESSION['RIGHTS'];
    $cmd->commandText = $sql;
    $res = $cmd->execute();
}
//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");?>
    
        <div id="content">
                <br/><br/>
               <form id="form1" name="form1" method="post" action="newAnnouncement.php">
                  <input type="submit" name="button" id="button" value="Make Announcement" />
                </form>
                <table>
                    <tr>
                        <th>Date</th>
                        <th>Subject</th>
                        <th>Content</th>
                      <th></th>
                      <th></th>
                        
                  </tr>
                    <?php 
                         include('connect-db.php');
        
        // number of results to show per page
        $per_page = 3;
        
        // figure out the total pages in the database
        $result = mysql_query("SELECT * FROM tblannouncements");
        $total_results = mysql_num_rows($result);
        $total_pages = ceil($total_results / $per_page);

        // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)
        if (isset($_GET['page']) && is_numeric($_GET['page']))
        {
                $show_page = $_GET['page'];
                
                // make sure the $show_page value is valid
                if ($show_page > 0 && $show_page <= $total_pages)
                {
                        $start = ($show_page -1) * $per_page;
                        $end = $start + $per_page; 
                }
                else
                {
                        // error - show first set of results
                        $start = 0;
                        $end = $per_page; 
                }               
        }
        else
        {
                // if page isn't set, show first set of results
                $start = 0;
                $end = $per_page; 
        }
        
        // display pagination
        
       echo "<p><b>View Page:</b> ";
      for ($i = 1; $i <= $total_pages; $i++)
   		{
                echo "<a href='post.php?page=$i'>$i</a> ";
        }
        echo "</p>"; for ($i = $start; $i < $end; $i++)
        {
                // make sure that PHP doesn't try to show results that don't exist
                if ($i == $total_results) { break; }
        
                // echo out the contents of each row into a table
                echo "<tr>";
                echo '<td>' . mysql_result($result, $i, 'Date') . '</td>';
                echo '<td>' . mysql_result($result, $i, 'Subject') . '</td>';
                echo '<td value = "50">' . mysql_result($result, $i, 'Content') . '</td>';
               echo '<td><a href="edit.php?id=' . mysql_result($result, $i, 'Aid') . '">Edit</a></td>';
                echo '<td><a href="delete.php?id=' . mysql_result($result, $i, 'Aid') . '">Delete</a></td>';
                echo "</tr>"; 
        }
                    ?>
          </table>                
            
      </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
<?endif;?>
