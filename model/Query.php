<?php

class Class_Query{
	
	public $count;
	private $connection;
	private $cmd;	
	
	public function __construct($sql){
		$conObj = new Class_SqlConnection();
		$this->connection = $conObj->connect();
		$this->cmd = new Class_SqlCommand($this->connection);
		$this->cmd->commandText = $sql;
		$res = $this->cmd->execute();
		$this->count = $res[0]['count'];
	}
	
	public function execute($sql){
		$$this->cmd->commandText = $sql;
		$res = $this->cmd->execute();
		return $res;
	}
	
	
}
