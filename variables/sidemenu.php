<?php
require_once('define.php');

$expIds = array();
$ids = array();
$nextIds = array();

$tbl = $_SESSION['RIGHTS']==1?TBL_DENTAL:TBL_MEDICAL;
$conObj = new Class_SqlConnection();
$con = $conObj->connect();
$cmd = new Class_SqlCommand($con,"");
$cmd->commandText = "SELECT count(*) as hits FROM ". TBL_APPOINTMENT . " WHERE status = 'pending'";
$resHits =  $cmd->execute();

$cmd->commandText = "SELECT * FROM tblmedicines WHERE Department = " . $_SESSION['RIGHTS'];
$medRes = $cmd->execute();
$count = 0;
for($i=0; $i<count($medRes); $i++){
    $qty = $medRes[$i]['Quantity'];
    $con = $medRes[$i]['Consumed'];
    $min = $medRes[$i]['minQuantity'];
    $bal = $qty - $con;
    if($bal <= $min){
        $ids[] = $medRes[$i]['ID'];
        $count++;
    }
}

$cmd->commandText = "SELECT ID, Expiration FROM tblmedicines WHERE Department = " . $_SESSION['RIGHTS'];
$expRes = $cmd->execute();
$countExp = 0;
for($i=0; $i<count($expRes); $i++){
    $currentDate = strtotime(date('Y-m-d'));
    $expDate = strtotime($expRes[$i]['Expiration']);
    if($currentDate >= $expDate){
        $expIds[] = $expRes[$i]['ID'];
        $countExp++;
    }
}
 
$cmd->commandText = "SELECT ID, date(NextVisitDate) as nextdate FROM tblmed WHERE NextVisitDate IS NOT NULL";
$nextRes = $cmd->execute();
$countNext = 0;
for($i=0; $i<count($nextRes); $i++){
    $currentDate = strtotime(date('Y-m-d'));
    $nextDate = strtotime($nextRes[$i]['nextdate']);
    if($currentDate == $nextDate){
        $nextIds[] = $nextRes[$i]['ID'];
        $countNext++;
    }
}


$ids = implode(",", $ids);
$expIds = implode(",", $expIds);
$nextIds = implode(",", $nextIds);


?>

<div id="main-menu">
	<h3>Notifications</h3>
	<ul>
	    <?php if($resHits[0]['hits']>0):?>
		  <li><a href="listPending.php?rights=<?php echo $_SESSION['RIGHTS'];?>">Pending &nbsp;&nbsp;<span style="color:RED;">( <?php echo $resHits[0]['hits'];?> )</span></a></li>
		<?php endif;?>
		
        <?php if($count>0):?>
          <li><a href="medecineNotif.php?ids=<?php echo $ids;?>">Medicines &nbsp;&nbsp;<span style="color:RED;">( <?php echo $count;?> )</span></a></li>
        <?php endif;?>

        <?php if($countExp>0):?>
          <li><a href="medecineNotif.php?ids=<?php echo $expIds;?>">Expired Medicines &nbsp;&nbsp;<span style="color:RED;">( <?php echo $countExp;?> )</span></a></li>
        <?php endif;?>

        <?php if($countNext>0):?>
          <li><a href="NextVisitNotif.php?ids=<?php echo $nextIds;?>">Next Visit Date &nbsp;&nbsp;<span style="color:RED;">( <?php echo $countNext;?> )</span></a></li>
        <?php endif;?>
        
	</ul>

</div>

