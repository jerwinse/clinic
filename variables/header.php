<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252" />
	<meta name="description" content="" />
	<meta name="generator" content="HTML-Kit" />
	<title>PUPT Medical and Dental Information System</title>
	<link rel="stylesheet" type="text/css" href="css/layout.css" />
	<link rel="stylesheet" type="text/css" href="css/color.css" />
	<link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.16.custom.css">	
	
	<script src="js/jquery-1.6.2.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
	<script src="js/global.js" type="text/javascript"></script>
	<script src="js/ts_picker.js" type="text/javascript"></script>

    <script>
        $(function() {
            $( "#NextVisitDate" ).datepicker();
            $( "#NextVisitDate" ).change(function() {
                $( "#NextVisitDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
            });
        });        
    </script>		
    
</head>
