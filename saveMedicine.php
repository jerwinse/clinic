<?php

$res = array();
$res['message'] = "Error on saving!";

if(isset($_POST)){
    require_once("define.php");
    
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    
    $dept = isset($_POST['dept'])?$_POST['dept']:"";
    $date = isset($_POST['date'])?$_POST['date']:"";
    $name = isset($_POST['name'])?$_POST['name']:"";
    $quantity = isset($_POST['quantity'])?$_POST['quantity']:"";
    $consumed = isset($_POST['consumed'])?$_POST['consumed']:"";
    $minQuantity = isset($_POST['minQuantity'])?$_POST['minQuantity']:"";
    $dept = isset($_POST['dept'])?$_POST['dept']:"";
    $expiration = isset($_POST['expiration'])?$_POST['expiration']:"";

    $cmd->commandText = "select count(*)  as hits from tblmedicines where name like '{$name}'";
    $res = $cmd->execute();
    if($res[0]['hits']>0){
        $res['message'] = "Duplicate Entry";
    }else{
        $cmd->commandText = "INSERT INTO tblmedicines (Date, Name, Quantity, Consumed, minQuantity, Department, Expiration) values('{$date}', '{$name}', {$quantity}, {$consumed}, {$minQuantity}, {$dept}, '{$expiration}')";
        $cmd->execute();
        $res['message'] = "Saved!!";        
    }     
}

print_r(json_encode($res));

?>