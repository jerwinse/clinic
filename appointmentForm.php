<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}

//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");
        $currentHour = date('H');
        $hours = array("05"=>"5AM", "05:30"=>"5:30AM", "06"=>"6AM", "06:30"=>"6:30AM", "07"=>"7AM", "07:30"=>"7:30AM", "08"=>"8AM", "08:30"=>"8:30AM", "09"=>"9AM", "09:30"=>"9:30AM", "10"=>"10AM", "10:30"=>"10:30AM", "11"=>"11AM", "11:30"=>"11:30AM", "12"=>"12PM", "12:30"=>"12:30AM", 
                       "13"=>"1PM", "13:30"=>"13:30PM", "14"=>"2PM", "14:30"=>"2:30PM", "15"=>"3PM", "15:30"=>"3:30PM", "16"=>"4PM", "16:30"=>"4:30PM", "17"=>"5PM", "17:30"=>"5:30PM", "18"=>"6PM", "18:30"=>"6:30PM", "19"=>"7PM", "19:30"=>"7:30PM", "20"=>"8PM");
        $id = $date = $NextVisitDate = $userID = $Course = $Lname = $Fname = $Mname = $Complaints = $Treatment = "";
        if(isset($_GET['id'])){
            $conObj = new Class_SqlConnection();
            $con = $conObj->connect();
            $cmd = new Class_SqlCommand($con,"");
            $cmd->commandText = "select * from tblmed where ID = {$_GET['id']}";
            $res = $cmd->execute();
            $date = $res[0]['date'];
            $NextVisitDate = $res[0]['NextVisitDate'];
            $userID = $res[0]['userID'];
            $Course = $res[0]['Course'];
            $Lname = $res[0]['Lname'];
            $Fname = $res[0]['Fname'];
            $Mname = $res[0]['Mname'];
            $Complaints = $res[0]['Complaints'];
            $Treatment = $res[0]['Treatment'];
            $dept = $res[0]['Department'];
            $jsFunction = "global.updateAppointment();";
            $id = $_GET['id'];
        }
        else{
            $date = date('Y-m-d');
            $dept = $_SESSION['RIGHTS'];
            $jsFunction = "global.saveAppointment();";
        }
    
    ?>
    
        <div id="content">
            <br/><br/>
            <form id="form1" name="form1" method="post"></form>
            <input type="hidden" name="dept" id="dept" value="<?php echo $dept;?>" />
            <input type="hidden" name="id" id="id" value="<?php echo $id;?>" />
            <table style="width:500px;">
                <tr><th>Date</th><td><input type="txtbox" name="date" id="date" value="<?php echo $date;?>"/></td></tr>
                <tr><th>Next Visit Date</th>
                    <td><input type="text" id="NextVisitDate" name="NextVisitDate"/>
                        <b>Hour</b>
                        <select name="hour" id="hour" style="font-size:12px; font-family:Arial, Helvetica, sans-serif;">
                            <?php foreach($hours as $index => $val):?>
                                <option value="<?php echo $index;?>" <?php echo ($index==$currentHour)?"selected":"";?>><?php echo $val;?></option>
                            <?php endforeach;?>
                        </select>
                    </td></tr>
                <tr><th>userID</th><td><input type="txtbox" name="userID" id="userID" value="<?php echo $userID;?>"/></td></tr>
                <tr><th>Course</th><td><input type="txtbox" name="Course" id="Course" value="<?php echo $Course;?>"/></td></tr>
                <tr><th>Last Name</th><td><input type="txtbox" name="Lname" id="Lname" value="<?php echo $Lname;?>"/></td></tr>
                <tr><th>First Name</th><td><input type="txtbox" name="Fname" id="Fname" value="<?php echo $Fname;?>" /></td></tr>
                <tr><th>Middle Name</th><td><input type="txtbox" name="Mname" id="Mname" value="<?php echo $Mname;?>" /></td></tr>
                <tr><th>Complaints</th><td><input type="txtbox" name="Complaints" id="Complaints" value="<?php echo $Complaints;?>" /></td></tr>
                <tr><th>Treatment</th><td><input type="txtbox" name="Treatment" id="Treatment" value="<?php echo $Treatment;?>" /></td></tr>
                <tr><th>&nbsp;</th><td><input type="button" value="Save" onclick="<?php echo $jsFunction;?>" style="width:150px;"/></td></tr>
            </table>                
            </form>
        </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
    
<?endif;?>
