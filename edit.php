<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){    
	if(isset($_POST['usernameText']) && isset($_POST['passwordText'])){	
		$conObj = new Class_SqlConnection();
		$con = $conObj->connect();
		$cmd = new Class_SqlCommand($con);
		$username = strtolower(trim($_POST['usernameText']));
		$password = md5(trim($_POST['passwordText']));
		$sqlUsername = "SELECT isEnabled, department, fullname FROM users WHERE username = '{$username}' AND password = '{$password}'";
		$cmd->commandText = $sqlUsername;
		$resultAcount = $cmd->execute();
		
		if(!empty($resultAcount)){
			if($resultAcount['0']['isEnabled'] == "1"){
                $rights = $resultAcount['0']['department'];
				$_SESSION['SID'] = $password;
                $_SESSION['RIGHTS'] = $rights;
                $_SESSION['fullname'] = $resultAcount[0]['fullname'];
                # department
                # 1 = dental
                # 2 = medical
                $sorting = (isset($_GET['sort']))?" order by Course asc":"";
                switch ($_SESSION['RIGHTS']) {
                    case 1:
                        $accepted = "SELECT * FROM " . TBL_DENTAL. " WHERE status = 'accepted' {$sorting}";
                        $pending = "SELECT * FROM " . TBL_DENTAL. " WHERE status = 'pending'";
                        break;
                    
                    case 2:
                        $accepted = "SELECT * FROM " . TBL_MEDICAL. " WHERE status = 'accepted' {$sorting}";
                        $pending = "SELECT * FROM " . TBL_MEDICAL. " WHERE status = 'pending'";
                        
                        break;
                }
                $cmd->commandText = $accepted;
                $res = $cmd->execute(); 
                $cmd->commandText = $pending;
                $resPending = $cmd->execute();
			}
			else{
				$_SESSION['message'] = "Your account is disabled";
				header("Location:login.php");
			}
		}
		else{
			$_SESSION['message'] = "Invalid Username / Password";		
			header("Location:login.php");
		}
	}
	else{
		$_SESSION['message'] = "Enter Username / Password";
		header("Location:login.php");
	}
}
else {
    $conObj = new Class_SqlConnection();
    $con = $conObj->connect();
    $cmd = new Class_SqlCommand($con,"");
    # department
    # 1 = dental
    # 2 = medical
    $sorting = (isset($_GET['sort']))?" order by Course asc":"";
        
    switch ($_SESSION['RIGHTS']) {
        case 1:
            $accepted = "SELECT * FROM " . TBL_DENTAL. " WHERE status = 'accepted' {$sorting}";
            $pending = "SELECT * FROM " . TBL_DENTAL. " WHERE status = 'pending'";
            break;
        
        case 2:
            $accepted = "SELECT * FROM " . TBL_MEDICAL. " WHERE status = 'accepted' {$sorting}";
            $pending = "SELECT * FROM " . TBL_MEDICAL. " WHERE status = 'pending'";
            break;
    }
    $cmd->commandText = $accepted;
    $res = $cmd->execute();	
    $cmd->commandText = $pending;
    $resPending = $cmd->execute();
}
//else{
//	$_SESSION['message'] = "Enter Username / Password";
//	header("Location:login.php");
//}

?>





<?php
/* 
 EDIT.PHP
 Allows user to edit specific entry in database
*/

 // creates the edit record form
 // since this form is used multiple times in this file, I have made it a function that is easily reusable
 function renderForm($id, $Date, $Subject, $Content, $error)
 {
 ?>
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>	

<?php include_once('variables/tabmenu.php');?>
<h1 id="top"><?php echo $_SESSION['fullname'];?></h1> <?endif;?>
<div id="inside">
<?php include_once("variables/sidemenu.php");?>
<div id="content">
 <?php 
 // if there are any errors, display them
 if ($error != '')
 {
 echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';
 }
 ?> 
 
 <form action="" method="post">
 <input type="hidden" name="id" value="<?php echo $id; ?>"/>
 <div>
 <table width="400" align="center">
 <tr>
 <td>
 <strong>ID:</strong></td><td> <?php echo $id; ?></td>
 </tr><tr><td>
 <strong>Date Posted: </strong></td><td> <input type="text" name="Date" value="<?php echo $Date; ?>"/></td></tr>
 <tr><td>
 <strong>Subject: </strong></td><td> <input type="text" name="Subject" value="<?php echo $Subject; ?>"/></td></tr>
 <tr><td>
 <strong>Content: </strong></td><td> <textarea name="Content" cols="45" rows="5"/><?php echo $Content; ?></textarea></td></td>
<!-- <p>* Required</p>-->
 <tr>
 <td></td><td>
 <input type="submit" name="submit" value="Save Changes"></td></tr>
 </div>
 </form> 
 
 <?php
 }



 // connect to the database
 include('connect-db.php');
 
 // check if the form has been submitted. If it has, process the form and save it to the database
 if (isset($_POST['submit']))
 { 
 // confirm that the 'id' value is a valid integer before getting the form data
 if (is_numeric($_POST['id']))
 {
 // get form data, making sure it is valid
 $id = $_POST['id'];
 $Date = mysql_real_escape_string(htmlspecialchars($_POST['Date']));
 $Subject = mysql_real_escape_string(htmlspecialchars($_POST['Subject']));
 $Content = mysql_real_escape_string(htmlspecialchars($_POST['Content']));
 
 // check that firstname/lastname fields are both filled in
 if ($Date == '' || $Subject == '' || $Content == '')
 {
 // generate error message
 $error = 'ERROR: Please fill in all required fields!';
 
 //error, display form
 renderForm($id, $Date, $Subject, $Content, $error);
 }
 else
 {
 // save the data to the database
 mysql_query("UPDATE tblannouncements SET Date='$Date', Subject='$Subject', Content='$Content' WHERE Aid='$id'")
 or die(mysql_error()); 
 
 // once saved, redirect back to the view page
 header("Location: post.php"); 
 }
 }
 else
 {
 // if the 'id' isn't valid, display an error
 echo 'Error!';
 }
 }
 else
 // if the form hasn't been submitted, get the data from the db and display the form
 {
 
 // get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)
 if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)
 {
 // query db
 $id = $_GET['id'];
 $result = mysql_query("SELECT * FROM tblannouncements WHERE Aid=$id")
 or die(mysql_error()); 
 $row = mysql_fetch_array($result);
 
 // check that the 'id' matches up with a row in the databse
 if($row)
 {
 
 // get data from db
 $Date = $row['Date'];
 $Subject = $row['Subject'];
 $Content = $row['Content'];
 
 // show form
 renderForm($id, $Date, $Subject, $Content, '');
 }
 else
 // if no match, display result
 {
 echo "No results!";
 }
 }
 else
 // if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
 {
 echo 'Error!';
 }
 }
?>
	</div>
	</div><!-- end of inside -->
	
	
	
	<!-- Footer -->
	<?php include_once('variables/footer.php');?>
	</body>
	</html>
   