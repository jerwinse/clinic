<?php

require("fpdf.php");



class PDF extends FPDF {

//Page header method

        function Header() {



        $this->SetFont('Times','',12);

      
		
        $this->SetTextColor(0,0,255);

        $this->SetLineWidth(1);

        $this->Image('formheader.png',30,10,150,35);

      
		
        $this->Ln(40);



}



        //Page footer method

        function Footer()       {

        //Position at 1.5 cm from bottom

        $this->SetY(-15);

        $this->SetFont('Arial','I',8);

        $this->Cell(0,10,'PUP Taguig Campus - Page '

        .$this->PageNo().'/{nb}',0,0,'C');

        }
       


        function BuildTable($header,$data) {

        //Colors, line width and bold font

        $this->SetFillColor(122,3,3);

        $this->SetTextColor(255);

        $this->SetDrawColor(128,0,0);

        $this->SetLineWidth(.3);

        $this->SetFont('','B');

        //Header

        // make an array for the column widths

        $w=array(30,80,23,23,26);

        // send the headers to the PDF document

        for($i=0;$i<count($header);$i++)

        $this->Cell($w[$i],7,$header[$i],1,0,'C',1);

        $this->Ln();

        //Color and font restoration

        $this->SetFillColor(251,244,39);

        $this->SetTextColor(0);

        $this->SetFont('');



        //now spool out the data from the $data array

        $fill=true; // used to alternate row color backgrounds

        foreach($data as $row)

        {

        $this->Cell($w[0],6,$row[0],'LR',0,'C',$fill);

        // set colors to show a URL style link

        $this->SetTextColor(127,2,3);

        $this->SetFont('', 'I');

        $this->Cell($w[1],6,$row[1],'LR',0,'C',$fill);

        // restore normal color settings

        $this->SetTextColor(0);

        $this->SetFont('');

        $this->Cell($w[2],6,$row[2],'LR',0,'C',$fill);

        $this->SetTextColor(0);

        $this->SetFont('');

        $this->Cell($w[3],6,$row[3],'LR',0,'C',$fill);

        $this->SetTextColor(0);

        $this->SetFont('');

        $this->Cell($w[4],6,$row[4],'LR',0,'C',$fill);



        $this->Ln();

        // flips from true to false and vise versa

        $fill =! $fill;

        }

        $this->Cell(array_sum($w),10,'','T');
		

        }

}



//connect to database

$connection = mysql_connect("localhost","root", "");

$db = "dbclinic";

mysql_select_db($db, $connection)

        or die( "Could not open $db database");





$sql = 'select * from tblmed order by Date';

$result = mysql_query($sql, $connection)

        or die( "Could not execute sql: $sql");



// build the data array from the database records.

While($row = mysql_fetch_array($result)) {

        $data[] = array($row['Date'], $row['Med'], $row['Qty'], $row['Consumed'], $row['Bal'] );

}



// start and build the PDF document

$pdf = new PDF();



//Column titles

$header=array('Date','Medicine Type','Qty','Cns','Bal');



$pdf->SetFont('Arial','',14);

$pdf->AddPage();

// call the table creation method

$pdf->BuildTable($header,$data);

//$pdf->Cell(20,100,'Prepared by: Ms. Joyce S. Reyes',0,0,'L');

$pdf->Output();
?>