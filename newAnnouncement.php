<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}

//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");?>

<?php
/* 
 NEW.PHP
 Allows user to create a new entry in the database
*/
 
 // creates the new record form
 // since this form is used multiple times in this file, I have made it a function that is easily reusable
 function renderForm($Date, $Subject, $Content, $error)
 {
 ?>
 
 <?php 
 // if there are any errors, display them
 if ($error != '')
 {
 echo '<div style="padding:4px; border:1px solid red; color:red;">'.$error.'</div>';
 }
 ?> 
 
 <form action="" method="post">
<form name="form1" method="post" action="">
<table width="600" align="center">

<tr>
<td>
<label>*Date Posted: </label></td>
<td>
<input type="text" name="date" value="<?php echo $Date; ?>" /></td>
</tr>
<tr>
<td>
<label>*Subject: </label></td>
<td>
<input type="text" name="subject" value="<?php echo $Subject; ?>" /></td></tr>
<tr>
<td>
<label>*Content: </label>
</td><td>
<!--<input type="text" name="content" value="<?//php echo $Content; ?>" />-->

<textarea name="content" id="textarea" cols="45" rows="5"><?php echo $Content; ?></textarea>

<!--<textarea name="ff" id="content" cols="45" rows="5"><?php echo $Content; ?></textarea>
<!--<input type="text" name="atextfield" value="<?//php echo $line['Content'];?>" /></td></tr>-->
<tr>
<td></td>
<td><input type="submit" name="submit" value="Save"></td>
</tr>
</table>

 
 </div>
 </form> 
 </body>
 </html>
 <?php 
 }
 
 
 

 // connect to the database
 include('connect-db.php');
 
 // check if the form has been submitted. If it has, start to process the form and save it to the database
 if (isset($_POST['submit']))
 { 
 // get form data, making sure it is valid
 $Date = mysql_real_escape_string(htmlspecialchars($_POST['date']));
 $Subject = mysql_real_escape_string(htmlspecialchars($_POST['subject']));
 $Content = mysql_real_escape_string(htmlspecialchars($_POST['content']));
 
 // check to make sure both fields are entered
 if ($Date == '' || $Subject == '' || $Content == '')
 {
 // generate error message
 $error = 'ERROR: Please fill in all required fields!';
 
 // if either field is blank, display the form again
 renderForm($Date, $Subject, $Content, $error);
 }
 else
 {
 // save the data to the database
 mysql_query("INSERT tblannouncements SET Date='$Date', Subject='$Subject', Content='$Content'")
 or die(mysql_error()); 
 
 // once saved, redirect back to the view page
 header("Location: post.php"); 
 }
 }
 else
 // if the form hasn't been submitted, display the form
 {
 renderForm('','','','');
 }
?> 

  </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    <?endif;?>