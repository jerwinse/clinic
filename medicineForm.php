<?php include_once('define.php');?>

<?php

session_start();

if(!isset($_SESSION['SID'])){
    $_SESSION['message'] = "Enter Username / Password";
    header("Location:login.php");
}

//else{
//  $_SESSION['message'] = "Enter Username / Password";
//  header("Location:login.php");
//}

?>


<!-- HEADER -->
<?php include_once('variables/header.php');?>

<body>
<?if($_SESSION['SID']):?>   
    <!-- Tab Menu -->
    <?php include_once('variables/tabmenu.php');?>
    
    <h1 id="top"><?php echo $_SESSION['fullname'];?></h1>
    
    <div id="inside">
    
    <!-- Side Menu -->
    <?php include_once("variables/sidemenu.php");
        $id = $date = $name = $quantity = $consumed = $minQuantity = $expiration = $dept = "";
        if(isset($_GET['id'])){
            $conObj = new Class_SqlConnection();
            $con = $conObj->connect();
            $cmd = new Class_SqlCommand($con,"");
            $cmd->commandText = "select * from tblMedicines where ID = {$_GET['id']}";
            $res = $cmd->execute();
            $date = $res[0]['Date'];
            $name = $res[0]['Name'];
            $quantity = $res[0]['Quantity'];
            $consumed = $res[0]['Consumed'];
            $minQuantity = $res[0]['minQuantity'];
            $expiration= $res[0]['Expiration'];
            $dept = $res[0]['Department'];
            $jsFunction = "global.updateMedicine();";
            $id = $_GET['id'];
        }
        else{
            $date = date('Y-m-d');
            $dept = $_SESSION['RIGHTS'];
            $jsFunction = "global.saveMedicine();";
        }
    
    ?>
    
        <div id="content">
            <br/><br/>
            <form method="post"></form>
            <input type="hidden" name="dept" id="dept" value="<?php echo $dept;?>" />
            <input type="hidden" name="id" id="id" value="<?php echo $id;?>" />
            <table style="width:500px;">
                <tr><th>Date</th><td><input type="txtbox" name="date" id="date" value="<?php echo $date;?>"/></td></tr>
                <tr><th>Name</th><td><input type="txtbox" name="name" id="name" value="<?php echo $name;?>"/></td></tr>
                <tr><th>Quantity</th><td><input type="txtbox" name="quantity" id="quantity" value="<?php echo $quantity;?>" /></td></tr>
                <tr><th>Consumed</th><td><input type="txtbox" name="consumed" id="consumed" value="<?php echo $consumed;?>" /></td></tr>
                <tr><th>Min. Quantity</th><td><input type="txtbox" name="minQuantity" id="minQuantity" value="<?php echo $minQuantity;?>" /></td></tr>
                <tr><th>Expiration</th><td><input type="txtbox" name="expiration" id="expiration" value="<?php echo $expiration;?>" /></td></tr>
                <tr><th>&nbsp;</th><td><input type="button" value="Save" onclick="<?php echo $jsFunction;?>" style="width:150px;"/></td></tr>
            </table>                
            </form>
        </div>
    </div><!-- end of inside -->
    
    
    
    <!-- Footer -->
    <?php include_once('variables/footer.php');?>
    </body>
    </html>
    
<?endif;?>
