<?php

require_once("class/SqlConnection.php");
require_once("class/SqlCommand.php");
require_once("class/Paging.php");
require_once("model/Query.php");

# database details
define('DBHOST','localhost');
define('DBUSERNAME','root');
define('DBPASSWORD','password');
define('DBNAME','dbclinic');
define('PERPAGE', 20);
define('TBL_DENTAL', 'tbldental');
define('TBL_MEDICAL', 'tblmedical');
define('TBL_MEDICINES', 'tblmedicines');
define('TBL_MED', 'tblmed');
define('TBL_APPOINTMENT', 'tblappointments');
